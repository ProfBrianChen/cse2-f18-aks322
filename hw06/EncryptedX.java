//Amanda Sax
//CSE 002
//HW 06
//October 23

import java.util.Scanner;//importing the scanner class

public class EncryptedX{//defining the class
  public static void main (String[] args){//defining the main
    Scanner scnr = new Scanner(System.in);//calling the scanner
    int numRows = 0;
    System.out.println("Please enter an integer from 1-10: ");
    boolean continueGoing = true;
    boolean numInput;
    while(continueGoing){//while statement
      numInput = scnr.hasNextInt();
      if(numInput){//wont be able to enter unless numInput is an int
        numRows = scnr.nextInt();//taking the question and stroing it into int numRows
        continueGoing = !continueGoing;    
      } 
      else{//enters here if the number entered is not an int
        scnr.next();
        System.out.println("Please enter a valid  # ");
      }
    }
    while(numRows < 0 || numRows > 100){
      //this is checking to make sure the value if between 0 and 100, and it wont go out of the loop until it is true
      System.out.println("Please enter the same integer from 1-10 ");
      numRows = scnr.nextInt();
    }
    
    for(int k = 0; k<=numRows; k++){//this prints the number of rows
      for(int m = 0; m<=numRows; m++){//this prints the number of columns
        if (k==m||k+m == numRows){
         //k is rows and m is columns, if the location on a column or a row is the same then a space should be printed
         //when you look at the placement of all the spaces on the far side, the column number and the row number 
         //always add up to the total numbers of rows so at that point you print a space
        System.out.print(" ");
        }
        else{//if it is anything other than those two options print a *
        System.out.print("*"); 
        }
      }
      System.out.println();  
   }
      
    }
  }