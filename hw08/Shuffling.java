//Amanda Sax
//HW08
//CSE002 
//Due November 15, 5pm


import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards); 
    System.out.println();
    shuffle(cards);
    System.out.println();
    System.out.println("Shuffled");
    printArray(cards); //displays cards after shuffled
    System.out.println();
    System.out.println();
    while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      System.out.println("Hand");
      printArray(hand);
      System.out.println();
      index = index - numCards;
      System.out.println();
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
  
  
  
  public static void shuffle(String[] cards){
    Random random = new Random();//Have to call the random class
    for(int i = 0; i<cards.length; i++){
      int newCards = random.nextInt(52);//Creating 52 random ints
      String temp = cards[i];// you have to store this into a temporary varible, so the value in here doesnt dissapear. 
      cards[i] = cards[newCards];// card[i] gets the value of newCards
      cards[newCards] = temp;//newCards gets the value that is stored in temp which is the value of cards[i] 
      //this is how you swap the values 
    }
  }
  
  public static String[] getHand(String[] cards, int index, int numCards){
    String[] newArray  = new String[numCards];
    //this is creating the new array that you are going to return and has the index of numCards
    for (int i = 0; i<5; i++){
      newArray[i] = cards[index - i];
      //newArray[i] gets the value of cards[index - i] which will continually decrment from the 51st car each time it goes through the for statement 
    }
    return newArray;//returning the new arrray that you made
    
  }
  
  public static void printArray(String[] cards){
    for( int i = 0; i < cards.length; i++){//you are just going through every value of the cards, and after each number is printed out, you are adding a space after the element in each index
      System.out.print(cards[i] + " ");
    }
  } 
}

