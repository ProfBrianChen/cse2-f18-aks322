//Amanda Sax
//CSE 002 
//hw02
//Due: September 11
//objective of giving you practice manipulating data stored in variables, 
//in running simple calculations, and in printing the numerical output that you generated.
//You go shopping at a store and want to compute the cost of the items you bought, including the PA sales tax of 6%.

public class Arithmetic{
  // defining the class
  public static void main (String[] args){
    //defining the main
        //Number of pairs of pants
        int numPants = 3;
        //Cost per pair of pants
        double pantsPrice = 34.98;

        //Number of sweatshirts
         int numShirts = 2;
        //Cost per shirt
         double shirtPrice = 24.99;

        //Number of belts
         int numBelts = 1;
        //cost per belt
          double beltCost = 33.99;

        //the tax rate
          double paSalesTax = 0.06;
    
    double totalSalesTaxOfPants = numPants * pantsPrice * paSalesTax;
    totalSalesTaxOfPants *= 100;
    totalSalesTaxOfPants = (int)totalSalesTaxOfPants;
    totalSalesTaxOfPants /= 100;
    //these past three steps have been so you can have the total be rounded off correctly
    System.out.println("The total sales tax charged for buying pants is $"+totalSalesTaxOfPants);
    // calculated the total sales tax for pants 
    
    double totalSalesTaxOfShirts = numShirts * shirtPrice * paSalesTax;
    totalSalesTaxOfShirts *= 100;
    totalSalesTaxOfShirts = (int)totalSalesTaxOfShirts;
    totalSalesTaxOfShirts /= 100; 
    //these past three steps have been so you can have the total be rounded off correctly
    System.out.println("The total sales tax charged for buying shirts is $"+totalSalesTaxOfShirts);
    // calculated the total sales tax for shirts 
    
    double totalSalesTaxOfBelts = numBelts * beltCost * paSalesTax;
    totalSalesTaxOfBelts *= 100;
    totalSalesTaxOfBelts = (int)totalSalesTaxOfBelts;
    totalSalesTaxOfBelts /= 100;
    //these past three steps have been so you can have the total be rounded off correctly
    System.out.println("The total sales tax charged for buying belts is $"+totalSalesTaxOfBelts);
    // calculated the total sales tax for belts 
    
    double totalCostOfPants = numPants * pantsPrice + totalSalesTaxOfPants;
    System.out.println("The total cost for pants is $" +totalCostOfPants);
    // caulculated the total cost of the pants 
    
    double totalCostOfShirts = numShirts * shirtPrice + totalSalesTaxOfShirts;
    System.out.println("The total cost for shirts is $" +totalCostOfShirts);
    // caulculated the total cost of the shirts 
    
    double totalCostOfBelts = numBelts * beltCost + totalSalesTaxOfBelts;
    System.out.println("The total cost for the belt is $" +totalCostOfBelts);
    //// caulculated the total cost of the belts 
    
    double totalCostBeforeTax = (numPants * pantsPrice) + (numShirts * shirtPrice) + (numBelts * beltCost);
    System.out.println("The total cost of the purchase before tax is $" +totalCostBeforeTax);
    //calculates the total cost of the purchase before tax
    
    double totalSalesTax = totalSalesTaxOfPants + totalSalesTaxOfShirts + totalSalesTaxOfBelts;
    System.out.println("The total sales tax paid is $" +totalSalesTax);
    //calculates the total amount of sales tax paid
    
    double totalOfTransaction = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    System.out.println("The Total Cost of the Transaction with tax is $" +totalOfTransaction);
    // calculates the total cost of the transaction
    
  }//closing the main
}//closing the class