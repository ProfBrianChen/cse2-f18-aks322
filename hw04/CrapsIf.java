//Amanda Sax
//CSE 002
//HW 04
//CrapsIf
//Due Septmber 25th

import java.util.Scanner;//importing the scanner class 
import java.util.Random;//importing the random class

public class CrapsIf{//defining the class
    public static void main (String[] args){//defining the main
      Scanner scnr = new Scanner(System.in);//calling the scanner
      Random randGen = new Random();//have to import the random class so i can use it
      System.out.println("Would you like to randomly cast dice or would you like to state the two dice you want to evaluate?");
      System.out.println("Enter Randomly or State dependng on what you want");//asking a question
      String userDecision  = scnr.next();//recording the responce from that question 
      int one;
      //the ints are what we are going to use throughout the code to hold the numbers that we are putting into the swtich statement 
      //have to define them here so they are outside of the swtich. 
      int two;
      
      if ( userDecision.equals("Randomly")){
        //randomly has to be in quotes because it is a string and userDecision is also a string
        //have to use the .equals since it is a string, can't use == because it isnt an int
           one = randGen.nextInt(6) + 1;//1 to 6 inclusive
           two = randGen.nextInt(6) + 1;//1 to 6 inclusive
           //System.out.println(+one); -- put these in to tes what numbers where coming out
          // System.out.println(+two);
      }
      
     else{
      System.out.println("Please input a number from 1-6");// if selected 'state', have to get two more #'s
      one = scnr.nextInt();//taking in which # said
      System.out.println("Please input another number from 1-6");
      two = scnr.nextInt();
     }
      
      //a bunch of if statements that say if you are this and that then this is what you are called
      //many of them contain or statements because the two imputs are symetrical.
      if (one==1 && two==1){
        System.out.println("Snake Eyes");
      }
      if ((one==1 && two==2)||(one==2 && two==1)){
        System.out.println("Ace Deuces");
      }
      if ((one==1 && two==3)||(one==3 && two==1)){
        System.out.println("Easy Four");
      }
      if ((one==1 && two==4)||(one==4 && two==1)){
        System.out.println("Fever Five");
      }
      if ((one==1 && two==5)||(one==5 && two==1)){
        System.out.println("Easy Six");
      }
      if ((one==1 && two==6)||(one==6 && two==1)){
        System.out.println("Seven Out");
      }
      if ((one==2 && two==2)||(one==2 && two==2)){
        System.out.println("Hard Four");
      }
    if ((one==2 && two==3)||(one==3 && two==2)){
       System.out.println("Fever Five");
     }
      if ((one ==2 && two==4)||(one==4 && two ==2)){
        System.out.println("Easy Six");
      }
      if ((one==2 && two==5)||(one==5 && two==2)){
        System.out.println("Seven Out");
      }
      if ((one==2 && two==6)||(one==6 && two==2)){
        System.out.println("Easy Eight");
      }
      if ((one==3 && two==3)||(one==3 && two==3)){
        System.out.println("Hard Six");
      }
      if ((one==3 && two==4)||(one==4 && two==3)){
        System.out.println("Seven Out");
      }
      if ((one==3 && two==5)||(one==5 && two==3)){
        System.out.println("Easy Eight");
      }
      if ((one==3 && two==6)||(one==6 && two==3)){
        System.out.println("Nine");
      }
      if ((one==4 && two==4)||(one==4 && two==4)){
          System.out.println("Hard Eight");
      }
      if ((one==4 && two==5)||(one==5 && two==4)){
        System.out.println("Nine");
      }
      if ((one==4 && two==6)||(one==6 && two==4)){
        System.out.println("Easy Ten");
      }
      if ((one==5 && two==5)||(one==5 && two==5)){
        System.out.println("Hard Ten");
      }
      if ((one==5 && two==6)||(one==6 && two==5)){
        System.out.println("Yo-leven");
      }
      if ((one==6 && two==6)||(one==6 && two==6)){
        System.out.println("Boxcars");
      }     
      
    }//end the main
}//end the class