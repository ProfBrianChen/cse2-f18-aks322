//Amanda Sax
//CSE 002
//HW 04
//CrapsSwitch
//Due Septmber 25th

import java.util.Scanner;//importing the scanner class 
import java.util.Random;//importing the random class

public class CrapsSwitch{//defining the class
    public static void main (String[] args){//defining the main
      Scanner scnr = new Scanner(System.in);//calling the scanner
      Random randGen = new Random();//calling the random class
      System.out.println("Would you like to randomly cast dice or would you like to state the two dice you want to evaluate?");
      System.out.println("Enter Randomly or State dependng on what you want");//asking a question
      String userDecision  = scnr.next();//recording the responce from that question 
      int one;
      //the ints are what we are going to use throughout the code to hold the numbers that we are putting into the swtich statement 
      //have to define them here so they are outside of the swtich. 
      int two;
     
      
      switch (userDecision){
        case "Randomly": 
        //randomly has to be in quotes because it is a string 
           one = randGen.nextInt(6) + 1;//1 to 6 inclusive
           two = randGen.nextInt(6) + 1;//1 to 6 inclusive
          break;//have to use so it will break out of this case
        
        default:
          System.out.println("Please input a number from 1-6");// if selected 'state', have to get two more #'s
          one = scnr.nextInt();//taking in which # said
          System.out.println("Please input another number from 1-6");
          two = scnr.nextInt();
         break;
      
    }
      
 //for the the swtich statements, I have a swtich statement inside of a switch statement. 
 //I did this so we could check in each of the oen numbers and get the given output for the other value of two 
   switch (one){
     case 1:
      switch (two){
        case 1: 
        System.out.println("Snake Eyes");
        break;
        case 2: 
        System.out.println("Ace Deuces");
        break;
        case 3: 
        System.out.println("Easy Four");
        break;
        case 4: 
        System.out.println("Fever Five");
        break;
        case 5: 
        System.out.println("Easy Six");
        break;
        case 6: 
        System.out.println("Seven out");
        break;}
      break;
      case 2:
        switch (two){
          case 1: 
          System.out.println("Ace Duece");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3:
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6:                
          System.out.println("Easy Eight");
          break;}
      break;
     case 3:
        switch (two){
          case 1: 
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4:
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:                
          System.out.println("Nine");
          break;}
      break;
     case 4:
        switch (two){
          case 1: 
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          break;
          case 6:                
          System.out.println("Easy Ten");
          break; }
      break;
      case 5:
        switch (two){
          case 1: 
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:                
          System.out.println("Yo-leven");
          break;}
      break;
      case 6:
        switch (two){
          case 1: 
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Nine");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:                
          System.out.println("Boxcars");
          break;}
      break;
      }
      
    }//end main method
}//end class