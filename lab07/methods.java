//Amanda Sax
//Lab07
//CSE002
//November 2

import java.util.Scanner;
import java.util.Random;

public class methods{
  public static void main(String[] args){
    Scanner scnr = new Scanner(System.in);
    boolean valid = true;
    while (valid){
      System.out.println("would you like to generate a sentence? Yes or no?");
      String yesOrNo = scnr.nextLine();
      if(yesOrNo.equals("yes")){//will go into here until it is entered that they dont want to generate a sentence
        System.out.print("The ");
        NounSubject() ;
        System.out.print("is ");
        Adjective() ;
        System.out.print("and has "); 
        PastTenseVerb();
        System.out.print("although "); 
        NounObject();
        System.out.print("is " );
        Adjective();
        System.out.println(" ");
        
      }
      else {
        valid = false;
      }
    }
    
    String NounSubject = thesisStatement();//this is taking the Nounsubject and making it a string so it can be used in other methods
    generateParagraph(NounSubject);//each of these call the method
    lastSentence(NounSubject);
    createParagraph(NounSubject);
    
  } // End of Main Method
  
  
  public static void Adjective(  ){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String adjective;
    //using a switch statement for the random values to get the adjective that corresponds with that number
    switch (randomInt){
      case 0: adjective = "0";
      System.out.print("sticky ");
      break;
      case 1: adjective = "1";
      System.out.print("bright ");
      break;
      case 2: adjective = "2";
      System.out.print("dependant ");
      break;
      case 3: adjective = "3";
      System.out.print("sweet ");
      break;
      case 4: adjective = "4";
      System.out.print("present ");
      break;
      case 5: adjective = "5";
      System.out.print("hollow ");
      break;
      case 6: adjective = "6";
      System.out.print("quiet ");
      break;
      case 7: adjective = "7";
      System.out.print("married ");
      break;
      case 8: adjective = "8";
      System.out.print("young ");
      break;
      case 9: adjective = "9";
      System.out.print("necessary ");
      break;
    }
  }
  
  public static void NounSubject(  ){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    //using a switch statement for the random values to get the nounsubject that corresponds with that number
    String nounsubject;
    switch (randomInt){
      case 0: nounsubject = "0";
      System.out.print("car ");
      break;
      case 1: nounsubject = "1";
      System.out.print("bridge ");
      break;
      case 2: nounsubject = "2";
      System.out.print("man ");
      break;
      case 3: nounsubject = "3";
      System.out.print("town ");
      break;
      case 4: nounsubject = "4";
      System.out.print("water ");
      break;
      case 5: nounsubject = "5";
      System.out.print("metal ");
      break;
      case 6: nounsubject = "6";
      System.out.print("mountain ");
      break;
      case 7: nounsubject = "7";
      System.out.print("state ");
      break;
      case 8: nounsubject = "8";
      System.out.print("country ");
      break;
      case 9: nounsubject = "9";
      System.out.print("beach ");
      break;
    }
    
    
    // return  ;
  }
  
  public static void PastTenseVerb(  ){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    //using a switch statement for the random values to get the randomVerb that corresponds with that number
    String randomVerb;
    switch (randomInt){
      case 0: randomVerb = "0";
      System.out.print("beat ");
      break;
      case 1: randomVerb = "1";
      System.out.print("became ");
      break;
      case 2: randomVerb = "2";
      System.out.print("bent ");
      break;
      case 3: randomVerb = "3";
      System.out.print("caught ");
      break;
      case 4: randomVerb = "4";
      System.out.print("cost ");
      break;
      case 5: randomVerb = "5";
      System.out.print("drew ");
      break;
      case 6: randomVerb = "6";
      System.out.print("fell ");
      break;
      case 7: randomVerb = "7";
      System.out.print("put ");
      break;
      case 8: randomVerb = "8";
      System.out.print("kept ");
      break;
      case 9: randomVerb = "9";
      System.out.print("ran ");
      break;
    }
    //break;
    //return randomVerb;
  }
  
  public static void NounObject(  ){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    //using a switch statement for the random values to get the randomObjectNoun that corresponds with that number
    String randomObjectNoun;
    switch (randomInt){
      case 0: randomObjectNoun = "0";
      System.out.print("him ");
      break;
      case 1: randomObjectNoun = "1";
      System.out.print("her ");
      break;
      case 2: randomObjectNoun = "2";
      System.out.print("it ");
      break;
      case 3: randomObjectNoun = "3";
      System.out.print("them ");
      break;
      case 4: randomObjectNoun = "4";
      System.out.print("us ");
      break;
      case 5: randomObjectNoun = "5";
      System.out.print("whom ");
      break;
      case 6: randomObjectNoun = "6";
      System.out.print("his ");
      break;
      case 7: randomObjectNoun = "7";
      System.out.print("those ");
      break;
      case 8: randomObjectNoun = "8";
      System.out.print("that ");
      break;
      case 9: randomObjectNoun = "9";
      System.out.print("us ");
      break;
    }
  }
  
  
  public static String thesisStatement( ){//this is the thesis statement
    Scanner scnr = new Scanner(System.in);
    String NounSubject = " ";
    System.out.println("would you like to generate a sentence in order to generate a paragraph? Yes or no?");
    String yesOrNo = scnr.nextLine();
    if(yesOrNo.equals("yes")){
      System.out.print("The ");
      NounSubject();
      System.out.println("<- please enter the word just displayed");
      //instructions said to store it into a string so this is how i did it
      NounSubject = scnr.nextLine();
      System.out.print("The " +NounSubject);//then put it here so it is all in one line
      System.out.print(" is ");
      Adjective() ;
      System.out.print("and has "); 
      PastTenseVerb();
      System.out.print("although "); 
      NounObject();
      System.out.print("is " );
      Adjective();
      System.out.println(" ");
      System.out.println(" ");
    }
    else {
    }
    return NounSubject; 
    
  }
  
  public static void generateParagraph(String NounSubject){
    System.out.print("The ");
    System.out.print(NounSubject+ " ");//this is the subject of the previous sentence and of this sentence
    System.out.print("is ");
    Adjective() ;
    System.out.print("and has "); 
    PastTenseVerb();
    System.out.print("although "); 
    NounObject();
    System.out.print("is " );
    Adjective();
    System.out.println(" ");
  }
  public static void lastSentence(String NounSubject){//the parameter is the NounSubject
    System.out.print("That ");
    System.out.print(NounSubject+ " ");//the line that says that it is generating the conclusion about the subject
    System.out.print("knew ");
    Adjective() ;
    System.out.print("her ");
    NounObject();
    System.out.println(" ");
    System.out.println(" ");//So there is a gap between this and the pargaraph
  }
  
  public static void createParagraph(String NounSubject){//has NounSubject As parameter
    //calls all the other methods in the document
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    for(randomInt=0; randomInt<10; randomInt++){
      System.out.print("The ");
      NounSubject() ;
      System.out.print("is ");
      Adjective() ;
      System.out.print("and has "); 
      PastTenseVerb();
      System.out.print("although "); 
      NounObject();
      System.out.print("is " );
      Adjective();
      System.out.println(" ");
    }
    generateParagraph(NounSubject);//the nounSubject comes from the thesis statement
    lastSentence(NounSubject);
    
  }
  
}