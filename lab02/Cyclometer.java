//Amanda Sax
// September 7 2018
//CSE 002
// THe purpose of this code is to record two kinds of data, 
//the time elapsed in seconds, and the number of rotations of the front wheel during that time. 
// We are supposed to do this by 
//1. print the number of minutes for each trip
//2. print the number of counts for each trip
//3. print the distance of each trip in miles
//4. print the distance for the two trips combined

public class Cyclometer {
  //defining class
  public static void main(String args[]){
    //defining main method
      int secsTrip1=480;  //this is the input for the secsTrip1
      int secsTrip2=3220;  //this is the input for the secsTrip2
	    int countsTrip1=1561;  //this is the input for the countsTrip2
	    int countsTrip2=9037; //this is the input for the countsTrip1
     
      double wheelDiameter=27.0,  // creating a variable for wheelDiameter
  	  PI=3.14159, //creating a variable for PI
  	  feetPerMile=5280,  //creating a variable that represents how many feetPerMile
  	  inchesPerFoot=12,   //creating a variable that represents how many inchesPerFoot
  	  secondsPerMinute=60;  //creating a variable that represents how many secondsPerMinute
	    double distanceTrip1, distanceTrip2,totalDistance;  // defining all of these as doubles
      
      System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+  countsTrip1+" counts.");
      //printing how many minutes and counts trip 1 had
	    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
      //printing how many minutes and counts trip 2 had
    
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// gives distance in feet/mi
	    totalDistance=distanceTrip1+distanceTrip2;//the total distance of the two trips
    
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
    	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

  }//end of main
}//end of class

