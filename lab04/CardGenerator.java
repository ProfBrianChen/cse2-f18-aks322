//Amanda Sax
//CSE 002
//Lab04
//The goal of this lab is use a random number generator to select numbers from 1-52
// for a magician so he can practice his tricks on his own.
//I will do this by generating a random number, creating two string variables, using if statements toassign the suit name,
//using a swtich statement to assign the card identity, and printing out the name of the randomly selected card

import java.util.Random;//importing the random class

public class CardGenerator{
  //defining the class
    public static void main (String[] args){
      //defining the main
      Random randGen = new Random();//calling the random class
      //int upperBound = 52;
     // int baseNum = 1;
     // int card = (int)(Math.random()*(upperBound+1))+baseNum;
      //create a number between 1-14 and that will tell you what the # or the name of the card is
      int cardName = 0 ;//making a card name 
      cardName = (int)(Math.random()*51)+2;//giving the card name its value
      //this will tell you what the card suit is
      //String suitName;
      String cardIdentity;
      
      switch (cardName % 13) {//for each one, you need to get 13 #'s you take the remainder of whatever number is inputted to get it in regards to 13
      case 1: cardIdentity = "1";
          System.out.println("You picked the Ace ");
        break;
      case 2: cardIdentity = "2";
          System.out.println("You picked the Two ");
        break;
      case 3: cardIdentity = "3";
          System.out.println("You picked the Three ");
        break;
      case 4: cardIdentity = "4";
          System.out.println("You picked the Four ");
        break;
      case 5: cardIdentity = "5";
          System.out.println("You picked the Five ");
        break;
      case 6: cardIdentity = "6";
          System.out.println("You picked the Six ");
        break;
      case 7: cardIdentity = "7";
          System.out.println("You picked the Seven ");
        break;
      case 8: cardIdentity = "8";
          System.out.println("You picked the Eight ");
        break;
      case 9: cardIdentity = "9";
          System.out.println("You picked the Nine ");
        break;
      case 10: cardIdentity = "10";
          System.out.println("You picked the Ten ");
        break;
      case 11: cardIdentity = "11";
          System.out.println("You picked the Jack ");
        break;
      case 12: cardIdentity = "12";
          System.out.println("You picked the Queen ");
        break;
      case 13: cardIdentity = "13";
          System.out.println("You picked the King ");
        break;
    }//ends the switch statement    
      
      //use print and not println so it all prints out on the same line. 
      if(cardName <= 13){
        System.out.print("of Diamond");
      }
      else if (cardName >=14 && cardName <= 26){
        System.out.print("of Clubs");
      }
      else if (cardName >=27 && cardName<=39){
        System.out.print("of Hearts");
      }
      else if (cardName >=40 && cardName<=52){
        System.out.print("of Spades");
      }
      
       
        
    }//ends the main
}//ends the class