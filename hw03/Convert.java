//Amanda Sax
//CSE 002 
//hw03
//Due: September 18
//Write a program that asks the user for doubles that represent the number of acres of 
//land affected by hurricane precipitation and how many inches of rain were dropped on average.

import java.util.Scanner;//importing the scanner

public class Convert{
  //defining the class
    public static void main (String[] args){
      //defining the main 
        Scanner myScanner;//declaring an instance of the object
        myScanner = new Scanner(System.in);//constructing an instance 
	      System.out.println("Enter the affected area in acres (in the form xx.xx): ");
        //asking the user to say what the effected area is 
	      double areaInAcres = myScanner.nextDouble();
        //nextDouble() lets us use the imput from the above question
        System.out.println("Enter the rainfall in the affected area (in inches): ");
        // asking the use to say what the rainfall in the affected area is 
        double rainfallPerInch = myScanner.nextDouble();
        //nextDouble() lets us use the imput from the above question
        double Gallons = rainfallPerInch * areaInAcres;
        //assigning acre-inch to term gallons
        Gallons *= 27154.285714286;
        //the conversion factor for acre-inch to gallons
        double cubicMiles = Gallons * .000000000000908168;
        //converting the gallons to cubic miles, using the conversion factor
        System.out.println(+cubicMiles+ " Cubic Miles");
        //printing out cubic miles

    }//end main
}//end class