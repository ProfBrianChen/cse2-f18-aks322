//Amanda Sax
//CSE 002 
//hw03
//Due: September 18
//Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;//importing the scanner

public class Pyramid{
  //defining the class
    public static void main (String[] args){
      //defining the main 
      Scanner myScanner;//declaring an instance of the object
      myScanner = new Scanner(System.in); //constructing an instance 
      System.out.println("The square side of the pyramid is (input length): ");
      // asking the user what the square side of the pyramid is 
      double squareSide = myScanner.nextDouble();
      // nextDouble() lets me take the imput from the above question
      System.out.println("The height of the pyramid is (input height): ");
       // asking the user what the height of the pyramid is 
      double heightOfPyramid = myScanner.nextDouble();
       // nextDouble() lets me take the imput from the above question
      int totalVolume;
      //defining totalVolume
      totalVolume = (int)Math.pow(squareSide,2)*(int)(heightOfPyramid/3);
      //taking the imput that the user gives me and finding the volume. Explicitly casting as int, so there is no double
      System.out.println("The volume insdie the pyramid is: " +totalVolume+ ".");
      // prints the volume of the pyramid
      
      
    }//end main
     
}//end class
    
  
