//Amanda Sax
//CSE 002
//Lab05
//Due October 9th
//The goal of this code is to give practice with writing loops

import java.util.Scanner;//improting the scanner class

public class courses{//defining the class
  public static void main (String[] args){//defining the main
     Scanner scnr = new Scanner(System.in);//calling the scanner
     //System.out.println("What is the number of your course?");
     boolean continueGoing = true;
     boolean courseNumber;
     while(continueGoing){//while statement
       courseNumber = scnr.hasNextInt();
       if(courseNumber){
         int x= scnr.nextInt();
         System.out.println("The number of your course is "+x);
         continueGoing = !continueGoing;
       }//closes if statement
       else{
         scnr.next();
         System.out.println("Please enter a valid course # ");
       }//closes else statmen    
       break;
     }//closes while statment 
    scnr.next();
    
    System.out.println("What is the department's name?");
    boolean departmentName;
    while(continueGoing){
      departmentName = scnr.hasNext();
      if(departmentName){
        String x = scnr.next();
        System.out.println("The name of the department is " +x);
        continueGoing = !continueGoing;
      }
      else{
         scnr.next();
         System.out.println("Please input a valid department name ");
      }
    }
    scnr.nextInt();
    
    System.out.println("How many times a week does your course meet?");
    boolean timesPerWeek;
    while(continueGoing){//while statement
      timesPerWeek = scnr.hasNextInt();
      if(timesPerWeek){
         int x= scnr.nextInt();
         System.out.println("Your class meets "+x+ "times per week");
         continueGoing = !continueGoing;
       }//closes if statement
       else{
         scnr.next();
         System.out.println("Please enter a valid course # ");
       }//closes else statment
      break;
     }//closes while statment 
    scnr.next();
    
    System.out.println("What time does your class start?");
    boolean startingTime;
    while(continueGoing){//while statement
      startingTime = scnr.hasNextDouble();
      if(startingTime){
         double x= scnr.nextDouble();
         System.out.println("Your class starts at "+x);
         continueGoing = !continueGoing;
       }//closes if statement
       else{
         scnr.next();
         System.out.println("Please enter a valid course # ");
       }//closes else statment
      break;
     }//closes while statment 
    scnr.next(); 
    
    System.out.println("What is your instructors name?");
    boolean instructorsName;
    while(continueGoing){
      instructorsName = scnr.hasNext();
      if(instructorsName){
        String x = scnr.next();
        System.out.println("Your instructors name is " +x);
        continueGoing = !continueGoing;
      }
      else{
         scnr.next();
         System.out.println("Please input a valid instructor name ");
      }
    }
    scnr.nextInt();
    
    System.out.println("How many students are there in your class?");
    boolean studentsPerClass;
    while(continueGoing){//while statement
      studentsPerClass = scnr.hasNextInt();
      if(studentsPerClass){
         int x= scnr.nextInt();
         System.out.println("There are "+x+ " students in your class");
         continueGoing = !continueGoing;
       }//closes if statement
       else{
         scnr.next();
         System.out.println("Please enter a valid number of students ");
       }//closes else statment
      break;
     }//closes while statment 
    scnr.next();
    
    
  }//closes main
}//closes class


