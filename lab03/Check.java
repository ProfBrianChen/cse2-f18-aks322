//Amanda Sax
//CSE 002
//Lab03
// The goal of this lab is to use a Scanner class to obtain from the user 
//about the original cost of the check, the percentage tip they wish to pay, 
//and the number of ways the check will be split. Then determine how much each person in the 
//group needs to spend in order to pay the check.

import java.util.Scanner;
//importing the scanner

public class Check{
  // defining the class
    public static void main (String[] args){
      //defining the main
      Scanner myScanner = new Scanner( System.in );
      //declaring instance of scanner object and call the scanner conductor
      System.out.print("Enter the original cost of the check in the form xx.xx: ");
      // asking the user the original cost of the check
      double checkCost = myScanner.nextDouble();
      //accepting the user input
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
      // prompt the user for the tip percentage that they wish to pay
      double tipPercent = myScanner.nextDouble();
      // accept the input.
      tipPercent /= 100; 
      //We want to convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");
      //ask how many people went to dinner
      int numPeople = myScanner.nextInt();
      //accepts the input
      double totalCost;
      double costPerPerson;
      int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople;
      //get the whole amount, dropping decimal fraction
      dollars=(int)costPerPerson;
       //get dimes amount, e.g., 
      // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
      //  where the % (mod) operator returns the remainder
      //  after the division:   583%100 -> 83, 27%5 -> 2 
      dimes=(int)(costPerPerson * 10) % 10;
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
      
    }//end of main
}//end of class