import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  
  public static int[] randomInput(){
    //The randomInput() method generates an array of 10 random integers between 0 to 9.  
    // Implement randomInput so that it fills the array with random integers and returns the filled array. 
    Random random = new Random(); 
    int randomNum0= random.nextInt(10);
    int randomNum1= random.nextInt(10);
    int randomNum2= random.nextInt(10);
    int randomNum3= random.nextInt(10);
    int randomNum4= random.nextInt(10);
    int randomNum5= random.nextInt(10);
    int randomNum6= random.nextInt(10);
    int randomNum7= random.nextInt(10);
    int randomNum8= random.nextInt(10);
    int randomNum9= random.nextInt(10);
    int[] Array = new int[10];
    Array[0] = randomNum0;
    Array[1] = randomNum1;
    Array[2] = randomNum2;
    Array[3] = randomNum3;
    Array[4] = randomNum4;
    Array[5] = randomNum5;
    Array[6] = randomNum6;
    Array[7] = randomNum7;
    Array[8] = randomNum8;
    Array[9] = randomNum9;
    
    return Array;
    
  }
  
  public static int[] delete(int[] list,int pos){
    int[] Array = new int[list.length - 1]; //creating the second array with one less 
    for (int i = 0; i < list.length; i++) {
      for (int k = 0; i < k; k++){//copy the elements into another array
      if (i == pos) { //if i is equal to the entered intereger 
        continue; 
      } 
      Array[k++] = list[i]; 
    }  
  }
    return Array; 
  }
  
  
  
  public static int[] remove(int[] list,int target){
    int count = 0;  
    for (int i = 0; i < list.length; i++){ 
//this says go through every element and if the value in that element is equal to I then add one to the counter
      if (i == target){ 
        count++; 
      } 
    } 
    
    if (count == 0){ 
//if at the end the counter is still equal to zero, return the initial array
      return list; 
    } 
    
    int[] remove1 = new int[list.length - count]; //however many times i == target, the list will subtract that number
//creating a new array that will be returned so a list will print out without the elements
    int count1 = 0; //need a new counter variable
    for (int j = 0; j < list.length; j++){
      if (j != target){ //
        remove1[count1] = j; 
        count1++; 
      } 
    }         
    list = null;//will remove that value from the array
    return remove1;//will print the new array without that value
    
    
  }
}

