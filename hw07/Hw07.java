import java.util.Scanner;
import java.lang.String;

public class Hw07{
  public static void main(String[] args){
    Scanner scnr = new Scanner(System.in);
    String userInput = sampleText();
    // setting sampleText to the return value of the method
    boolean valid = true;
    while (valid){
      String userChoice = printMenu();
      if (userChoice.equals("c")){
        getNumOfNonWSCharacters(userInput);
      }
      if (userChoice.equals("w")){
        getNumOfWords(userInput); 
      }
      if (userChoice.equals("f")){
        System.out.println("Enter a word to be found: ");
        String foundWord = scnr.nextLine();
        findText(userInput,foundWord);
      }
      if (userChoice.equals("r")){
        replaceExclamation(userInput);
      }
      if (userChoice.equals("s")){
        shortenSpace(userInput);
      }
      if (userChoice.equals("q")){
        valid = false;
      } 
    }
    
  }
  
  public static String sampleText(){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Please enter a sample text");
    String userInput = scnr.nextLine();
    System.out.println("You entered: " +userInput);
    return userInput;
  }
  
  public static String printMenu(){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Below is a menu, please decide which elements you would like to use ");
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option: ");
    String userChoice = scnr.next();
    return userChoice;//returing the outcome of what elements the user says they want to use
  }
  
  public static void getNumOfNonWSCharacters(String userInput) {//has the parameter userInput
    int count = 0;//have to define the counter outside the main
    //will use the counter to count the number of spaces 
    for (int i = 0; i <= userInput.length()-1; i++)
    {
      char a = userInput.charAt(i);
      if (a == ' ') continue; //this is saying if it reaches a space, then to not count it.
      count++;
    }
    int a = count;
    System.out.println("The Number of Charecters not including whitespaes is: " +a);
  }
  
  public static void getNumOfWords(String userInput){
    int count = 0;
    for (int i = 0; i <= userInput.length()-1; i++)//have the length-1 to make sure it isnt 
    {
      char a = userInput.charAt(i);//determined the character at the index of i 
      if (a == ' ') continue;
      count++;//if the charecter is not a space it will add 1 to the counter
    }
    int a = (userInput.length() - count);//takes the length and subtracts the amount characters there were
    //add one to this because there is the amount of spaces plus the one extra word
    a++;
    System.out.println("The number of words is: " +a); 
    
  }
  
  public static void findText(String userInput, String foundWord){
    int count = 0; 
    for (String word: userInput.split(" ")){//slip will cut the string into indvidual words
      if (word.equals(foundWord)){
        count++;
      }
    }
    System.out.println(foundWord+ " instances: " +count);
  }
  
  public static void replaceExclamation(String userInput){
    userInput = userInput.replace('!','.');
    System.out.println(userInput); 
  }
  
  public static void shortenSpace(String userInput) {
    while(userInput.contains("  ")){
      userInput = userInput.replace("  ", " ");
    }
    System.out.println(userInput);
    
    
  }
  
  
}